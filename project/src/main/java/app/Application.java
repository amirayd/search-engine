package app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import app.MainRunner;

@SpringBootApplication
@ComponentScan({"app","commons", "index", "services"})
public class Application implements CommandLineRunner {

	@Autowired
	private MainRunner runner;
	
    public static void main(String[] args) {
    	SpringApplication app = new SpringApplication(Application.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }
    
    @Override
    public void run(String... args) throws Exception {
    	runner.run(args);
    }

}
