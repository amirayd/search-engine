package app;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import commons.Constants;
import index.Classifier;
import index.Indexer;
import model.EngineContext;
import model.Evaluation;
import model.RawDoc;
import model.Result;
import services.InputProcessor;
import services.TokenManager;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ContextRunner {

	private EngineContext context;
	
	@Autowired
	private InputProcessor inputProcessor;
	
	@Autowired
	private TokenManager tokenManager;
	
	@Autowired
	private Indexer indexer;
	
	@Autowired
	private Classifier classifier;
	
	public void run() throws IOException {
		run(this.context);
	}
	
	public void run(EngineContext context) throws IOException {
		if (context == null) {
			throw new IllegalArgumentException("context is not defined");
		}
		this.context = context;
		
		// 1. pre-processing - reading docs
		preprocess();
		
		context.setAnalyzer(new StandardAnalyzer());
		context.setSim(new ClassicSimilarity());
				
		// 2. tokenize + remove stop words + stemming
		tokenManager.createTokenStreamsWithFilters(context, context.getTrainSet());
		tokenManager.createTokenStreamsWithFilters(context, context.getTestSet());
		
		// 3. indexing
		System.out.println("indexing...");
		indexer.init(context);
		index();
		
		// 4. classifying
		System.out.println("classifying...");
		classifier.init(context);
		List<Result> results = classify(context.getClasses());

		// 5. write results
		System.out.println("\nwriting results...");
		inputProcessor.writeDocsToCsv(Paths.get(context.getPaths().get(Constants.OUTPUT_FILE)), results);
		
		context.getAnalyzer().close();
	}

	private List<Result> classify(Map<String, String> classes) throws IOException {
		List<Result> results = null;
		try {
			results = classifier.classify();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, Evaluation> classesEvaluation = new HashMap<String, Evaluation>();
		for (final String cls: classes.keySet()) {
			classesEvaluation.put(cls, new Evaluation());
		}
		
		if (results != null) {
			for (final Result result: results) {
				if (result.getPredictedClassId().equals(result.getTrueClassId())) {
					classesEvaluation.get(result.getTrueClassId()).increaseTruePositive();
				}
				else {
					classesEvaluation.get(result.getPredictedClassId()).increaseFalsePositive();
					classesEvaluation.get(result.getTrueClassId()).increaseFalseNegative();
				}
			}
			
			float macroFScore = Evaluation.getMacroFScore(classesEvaluation);
			float microFScore = Evaluation.getMicroFScore(classesEvaluation);
			
			long totalCorrect = results.stream()
				.filter((item) -> item.getPredictedClassId().equals(item.getTrueClassId()))
				.count();
			System.out.println("Stop Words #: " + context.getNumOfStopWords());
			System.out.println("K: " + context.getK());
			System.out.println("Macro F-Score: " + macroFScore);
			System.out.println("Micro F-Score: " + microFScore);
			System.out.println("Total: " + results.size());
			System.out.println("Correct: " + totalCorrect);
		}
		
		return results;
	}

	private void preprocess() throws IOException {
		// load classes
		Map<String, String> classes = inputProcessor.readClasses(Paths.get(context.getPaths().get(Constants.CLASSES_FILE)));
		context.setClasses(classes);
		// read train set
		List<RawDoc> trainDocs = inputProcessor.readDocsFromCsv(Paths.get(context.getPaths().get(Constants.TRAIN_FILE)));
		List<RawDoc> randomTrainDocs = inputProcessor.randomSelection(trainDocs, Constants.NUM_OF_DOCS_TRAIN);
		context.setTrainSet(randomTrainDocs);
		// read test set
		List<RawDoc> testDocs = inputProcessor.readDocsFromCsv(Paths.get(context.getPaths().get(Constants.TEST_FILE)));
		List<RawDoc> randomTestDocs = inputProcessor.randomSelection(testDocs, Constants.NUM_OF_DOCS_TEST);
		context.setTestSet(randomTestDocs);
	}

	private void index() throws IOException {
		context.getTestSet().forEach(doc -> indexDoc(indexer, doc, "test"));
		context.getTrainSet().forEach(doc -> indexDoc(indexer, doc, "train"));
		indexer.close();
	}

	private RawDoc indexDoc(Indexer indexer, RawDoc doc, String type) {
		if (doc != null) {
			try {
				indexer.indexDoc(doc, type);
			} catch (java.text.ParseException | IOException e) {
				System.out.println("(indexDoc) Error:" + e.getMessage());
				e.printStackTrace();
			}
		}
		return doc;
	}

	public EngineContext getContext() {
		return context;
	}

	public void setContext(EngineContext context) {
		this.context = context;
	}
	
}
