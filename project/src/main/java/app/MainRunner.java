package app;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import commons.Constants;
import model.EngineContext;
import services.InputProcessor;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MainRunner {
	@Value("classpath:parameters.txt")
    private Resource parameters;
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Autowired
	private ContextRunner ctxRunner;
	
	@Autowired
	private InputProcessor inputProcessor;
	
	public void run(String[] args) throws IOException, ParseException {
		Map<String, String> params = inputProcessor.readParamsFile(
				Paths.get(parameters.getURI()));
		
		EngineContext context = new EngineContext();
		
		normalizePaths(params);
		// all related files
		context.setPaths(params);
		context.setK(Integer.parseInt(params.get(Constants.K)));
		context.setIndexPath(params.get(Constants.INDEX_PATH));
		
		// configurations
		final int numOfStopWOrds = Constants.NUM_OF_STOPWORDS;
		context.setNumOfStopWords(numOfStopWOrds);
		
		Date start = new Date();
		
		ctxRunner.run(context);
		
		System.out.println(new Date().getTime() - start.getTime() + " total milliseconds");
	}

	private void normalizePaths(Map<String, String> paths) {
		String key;
		for (Entry<String, String> entry : paths.entrySet()) {
			key = entry.getKey();
			if (key != null && key.endsWith("File") || key.endsWith("Path")) {
				Resource resource = resourceLoader.getResource("classpath:" + entry.getValue());
				if (resource != null) {
					try {
						entry.setValue(resource.getURI().toURL().getPath());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}						
				}
			}
		}
	}
}
