package model;

import org.apache.commons.csv.CSVRecord;

/**
 *  Represent a raw document
 */
public class Result {
	private String docId;
	private String trueClassId;
	private String predictedClassId;
	
	public Result(String docId, String trueClassId, String predictedClassId) {
		init(docId, trueClassId, predictedClassId);
	}
	
	public Result(CSVRecord csvRecord) {
		init(csvRecord.get(0), csvRecord.get(1), csvRecord.get(2));
	}
	
	public void init(String docId, String trueClassId, String predictedClassId) {
		setDocId(docId);
		setTrueClassId(trueClassId);
		setPredictedClassId(predictedClassId);
	}
	
	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getTrueClassId() {
		return trueClassId;
	}

	public void setTrueClassId(String classId) {
		this.trueClassId = classId;
	}

	public String getPredictedClassId() {
		return predictedClassId;
	}

	public void setPredictedClassId(String predictedClassId) {
		this.predictedClassId = predictedClassId;
	}

}

