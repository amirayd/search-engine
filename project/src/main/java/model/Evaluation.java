package model;

import java.util.Map;

public class Evaluation {
	private float truePositive;
	private float falsePositive;
	private float falseNegative;
	
	public Evaluation() {
		this.setTruePositive(0);
		this.setFalseNegative(0);
		this.setFalsePositive(0);
	}

	public float getTruePositive() {
		return truePositive;
	}

	public void setTruePositive(float truePositive) {
		this.truePositive = truePositive;
	}

	public float getFalsePositive() {
		return falsePositive;
	}

	public void setFalsePositive(float falsePositive) {
		this.falsePositive = falsePositive;
	}

	public float getFalseNegative() {
		return falseNegative;
	}

	public void setFalseNegative(float falseNegative) {
		this.falseNegative = falseNegative;
	}
	
	public void increaseTruePositive() {
		this.setTruePositive(this.getTruePositive() + 1);
	}
	
	public void increaseFalseNegative() {
		this.setFalseNegative(this.getFalseNegative() + 1);
	}
	
	public void increaseFalsePositive() {
		this.setFalsePositive(this.getFalsePositive() + 1);
	}
	
	public float getPrecision() {
		return (this.getTruePositive() / (this.getTruePositive() + this.getFalsePositive()));
	}
	
	public float getRecall() {
		return (this.getTruePositive() / (this.getTruePositive() + this.getFalseNegative()));
	}
	
	public float getFScore() {
		return ((2 * this.getPrecision() * this.getRecall()) / (this.getPrecision() + this.getRecall()));
	}
	
	public static float getMacroFScore(Map<String, Evaluation> classEvaluations) {
		float totalFScore = 0;
		for (Evaluation eval: classEvaluations.values()) {
			totalFScore += eval.getFScore();
		}
		return (totalFScore / classEvaluations.size());
	}
	
	public static float getMicroFScore(Map<String, Evaluation> classEvaluations) {
		float totalTP = 0;
		float totalFP = 0;
		float totalFN = 0;
		for (Evaluation eval: classEvaluations.values()) {
			totalTP += eval.getTruePositive();
			totalFP += eval.getFalsePositive();
			totalFN += eval.getFalseNegative();
		}
		float precision = (totalTP / (totalTP + totalFP));
		float recall = (totalTP / (totalTP + totalFN));
		return ((2 * precision * recall) / (precision + recall));
	}
}
