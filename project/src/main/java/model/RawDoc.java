package model;

import org.apache.commons.csv.CSVRecord;

/**
 *  Represent a raw document
 */
public class RawDoc {
	private String docId;
	private String trueClassId;
	private String title;
	private String content;
	
	public RawDoc(CSVRecord csvRecord) {
		fromCsv(csvRecord);
	}
	
	public void fromCsv(CSVRecord csvRecord) {
		setDocId(csvRecord.get(0));
		setTrueClassId(csvRecord.get(1));
		setTitle(csvRecord.get(2));
		setContent(csvRecord.get(3));
	}
	
	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTrueClassId() {
		return trueClassId;
	}

	public void setTrueClassId(String classId) {
		this.trueClassId = classId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}

