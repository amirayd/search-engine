package model;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.Similarity;

public class EngineContext {
	
	private int k;
	
	private List<RawDoc> trainSet;
	private List<RawDoc> testSet;
	
	private Map<String, String> classes;

	private Analyzer analyzer;
	private Map<String, String> paths;
	
	private Set<Object> stopWords;
	private int numOfStopWords;
	private Similarity sim;
	
	private String indexPath;
	
	private String retrievalAlgorithm;

	public Analyzer getAnalyzer() {
		return analyzer;
	}
	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}
	
	public Map<String, String> getPaths() {
		return paths;
	}
	public void setPaths(Map<String, String> paths) {
		this.paths = paths;
	}
	
	public int getNumOfStopWords() {
		return numOfStopWords;
	}
	public void setNumOfStopWords(int numOfStopWords) {
		this.numOfStopWords = numOfStopWords;
	}
	public Similarity getSim() {
		return sim;
	}
	public void setSim(Similarity sim) {
		this.sim = sim;
	}
	
	public String getRetrievalAlgorithm() {
		return retrievalAlgorithm;
	}
	public void setRetrievalAlgorithm(String retrievalAlgorithm) {
		this.retrievalAlgorithm = retrievalAlgorithm;
	}
	public String getIndexPath() {
		return indexPath;
	}
	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}
	public List<RawDoc> getTrainSet() {
		return trainSet;
	}
	public void setTrainSet(List<RawDoc> trainSet) {
		this.trainSet = trainSet;
	}
	public List<RawDoc> getTestSet() {
		return testSet;
	}
	public void setTestSet(List<RawDoc> testSet) {
		this.testSet = testSet;
	}
	public int getK() {
		return k;
	}
	public void setK(int k) {
		this.k = k;
	}
	public Set<Object> getStopWords() {
		return stopWords;
	}
	public void setStopWords(Set<Object> stopWords) {
		this.stopWords = stopWords;
	}
	public Map<String, String> getClasses() {
		return classes;
	}
	public void setClasses(Map<String, String> classes) {
		this.classes = classes;
	}
}
