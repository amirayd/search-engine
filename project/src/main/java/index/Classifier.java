package index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.classification.ClassificationResult;
import org.apache.lucene.classification.document.KNearestNeighborDocumentClassifier;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.BytesRef;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import commons.Constants;
import model.EngineContext;
import model.Result;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Classifier {
	private KNearestNeighborDocumentClassifier knnClassifier;
	
	private EngineContext context;
	
	private int minDocsFreq = 0;
	private int minTermFreq = 0;
	
	public void init(EngineContext context) {
		this.context = context;
	}
	
	public List<Result> classify() throws IOException, ParseException {
		Directory dir = Indexer.getIndexDir(context.getIndexPath());
		IndexReader indexReader = DirectoryReader.open(dir);
		IndexSearcher searcher = new IndexSearcher(indexReader);
		Similarity sim = context.getSim();
		if (sim  != null) {
			searcher.setSimilarity(sim);
		}
		QueryParser qp = new QueryParser("content", context.getAnalyzer());
		Query query = qp.parse(Indexer.TYPE + ":" + "train");
		knnClassifier = createClassifier(indexReader, sim, query);
		
		List<Result> results = new ArrayList<Result>();
		Query queryForTest = qp.parse(Indexer.TYPE + ":" + "test");
		TopDocs hits = searcher.search(queryForTest, Constants.NUM_OF_DOCS_TRAIN);
		for (ScoreDoc sd : hits.scoreDocs) {
			Document d = searcher.doc(sd.doc);
			List<ClassificationResult<BytesRef>> classes = knnClassifier.getClasses(d);
			ClassificationResult<BytesRef> classificationResult = classes.get(0);
			String predictedClass = new String(classificationResult.getAssignedClass().bytes, "UTF-8");
			String docId = d.get(Indexer.DOC_ID);
			String trueClassId = d.get(Indexer.CLASS_ID);
			results.add(new Result(docId, trueClassId.trim(), predictedClass.trim()));
		}
		return results;
	}

	private KNearestNeighborDocumentClassifier createClassifier(IndexReader indexReader, Similarity sim, Query query) {
		Map<String, Analyzer> field2analyzer = new HashMap<String, Analyzer>();
		field2analyzer.put(Indexer.BODY, context.getAnalyzer());
		KNearestNeighborDocumentClassifier knnClassifier = new KNearestNeighborDocumentClassifier(indexReader, sim,
				query, context.getK(), minDocsFreq, minTermFreq, Indexer.CLASS_ID,
				field2analyzer, Indexer.BODY);
		return knnClassifier;
	}
}
