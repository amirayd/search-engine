package index;

import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import model.EngineContext;
import model.RawDoc;

/**
 *	Holds the logic of indexing documents 
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Indexer {
	public static final String DOC_ID = "DocID";
	public static final String CLASS_ID = "ClassID";
	public static final String PREDICTED_CLASS_ID = "PredictedClassID";
	public static final String TITLE = "Title";
	public static final String BODY = "Body";
	public static final String TYPE = "Type";
	
	private IndexWriter writer;
	
	private EngineContext context;
	
	public void init(EngineContext context) {
		this.context = context;
		try {
			this.writer = createIndexerWriter(context.getIndexPath(), true);
		} catch (IOException e) {
			System.out.println("Failed to create writer...");
		}
	}

	public static Directory getIndexDir(String indexPath) throws IOException {
		Directory dir = null;
		try {
			dir = FSDirectory.open(Paths.get(indexPath));
		} catch (IOException e) {
			System.out.println("Could not create index dir:" + e.getMessage());
			throw e;
		}
		return dir;
	}
	
	public void close() throws IOException {
		if (this.writer != null) {
			this.writer.close();
		}
	}
	
	public void restore() throws IOException {
		this.writer = createIndexerWriter(context.getIndexPath(), false);
	}
	
	private IndexWriter createIndexerWriter(String indexPath, boolean create) throws IOException {
		Directory dir = getIndexDir(indexPath);
		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		Similarity sim = context.getSim();
		if (sim  != null) {
			iwc.setSimilarity(sim);
		}
		if (create) {
			iwc.setOpenMode(OpenMode.CREATE);
		} else {
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}

		// Optional: for better indexing performance, if you
		// are indexing many documents, increase the RAM
		// buffer. But if you do this, increase the max heap
		// size to the JVM (eg add -Xmx512m or -Xmx1g):
		//
		// iwc.setRAMBufferSizeMB(256.0);

		IndexWriter writer = new IndexWriter(dir, iwc);

		return writer;
	}

	public void indexDoc(RawDoc newDoc, String type) throws ParseException, IOException {
		Document document = new Document();

		document.add(new StringField(DOC_ID, newDoc.getDocId(), Field.Store.YES));
		document.add(new StringField(CLASS_ID, newDoc.getTrueClassId(), Field.Store.YES));
		document.add(new StringField(TITLE, newDoc.getTitle(), Field.Store.YES));
		document.add(new StringField(TYPE, type, Field.Store.YES)); // "test" or "train"
		
		document.add(new TextField(BODY, newDoc.getContent(), Field.Store.YES));
//		FieldType contentFieldType = new FieldType(TextField.TYPE_NOT_STORED);
//	    contentFieldType.setStoreTermVectors(true);
//	    contentFieldType.setStoreTermVectorPositions(true);
//	    contentFieldType.setStoreTermVectorPayloads(true);
//	    contentFieldType.setStoreTermVectorOffsets(true);
//	    contentFieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
//	    Field contentField = new Field(BODY, newDoc.getContent(), contentFieldType);
//	    document.add(contentField);
	    
		writer.addDocument(document);
	}

	public IndexWriter getWriter() {
		return writer;
	}

	public void setWriter(IndexWriter writer) {
		this.writer = writer;
	}
}
