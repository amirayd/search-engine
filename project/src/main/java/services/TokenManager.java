package services;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.HyphenatedWordsFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.springframework.stereotype.Service;

import model.EngineContext;
import model.RawDoc;

@Service
public class TokenManager {
	
	public void createTokenStreamsWithFilters(EngineContext context, List<RawDoc> docs) {
		if (docs != null) {
			docs.forEach(doc -> prepareDoc(doc, context));	
		}
	}

	private RawDoc prepareDoc(RawDoc doc, EngineContext context) {
		if (doc != null) {
			TokenStream formatted = tokenizeStopStem(doc.getContent(), context);
//			doc.setStream(formatted);
			doc.setContent(tokenStreamToString(formatted));
		}
		return doc;
	}
	
	public TokenStream tokenizeStopStem(String input, EngineContext context) {
        TokenStream tokenStream = context.getAnalyzer().tokenStream("content", input);
        CharArraySet charSet = (context.getStopWords() != null) ? CharArraySet.copy(context.getStopWords()) 
        		: CharArraySet.copy(StandardAnalyzer.STOP_WORDS_SET);
        tokenStream = new StandardFilter(tokenStream);
        tokenStream = new StopFilter(tokenStream, charSet);
        tokenStream = new HyphenatedWordsFilter(tokenStream);
        tokenStream = new PorterStemFilter(tokenStream);  
        
        return tokenStream;
	}
	
	public String countWordsInLine(String line, ConcurrentMap<String, Integer> words) {
		if (line != null && line.length() > 0) {
			String[] tokens = line.trim().split("[^a-zA-Z0-9']+");
			for (String token : tokens) {
				if (token.length() > 1) {
					words.compute(token.toLowerCase(), (k, v) -> v == null ? 1 : v + 1);
				}
			}
		}
	    return line;
	}
	
	public String tokenStreamToString(TokenStream tokenStream) {
		StringBuilder sb = new StringBuilder();
		// OffsetAttribute offsetAttribute = tokenStream.addAttribute(OffsetAttribute.class);
		CharTermAttribute charTermAttr = tokenStream.getAttribute(CharTermAttribute.class);

		try {
			tokenStream.reset();
			while (tokenStream.incrementToken()) {
				if (sb.length() > 0) {
					sb.append(" ");
				}
				sb.append(charTermAttr.toString());
			}
			tokenStream.end();
			tokenStream.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return sb.toString();
	}
	
}
