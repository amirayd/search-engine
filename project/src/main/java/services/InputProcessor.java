package services;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import model.RawDoc;
import model.Result;

@Service
public class InputProcessor {

	public Map<String, String> readParamsFile(Path paramsFile) throws IOException {
		try (Stream<String> stream = Files.lines(paramsFile)) {
			Map<String, String> params = new HashMap<String, String>();
			for (String line : (Iterable<String>) stream::iterator) {
				if (line != null && line.length() > 0) {
					String[] parts = line.split("=");
					if (parts.length > 0) {
						params.put(parts[0], parts[1]);
					}
				}
			}
			return params;
		}
	}
	
	public List<RawDoc> randomSelection(List<RawDoc> docs, int amount) {
		Random randomizer = new Random();
		int size = docs.size();
		List<RawDoc> randomSelected = new ArrayList<RawDoc>();
		for (int i = 0; i < amount; i++) {
			randomSelected.add(docs.get(randomizer.nextInt(size)));
		}
		return randomSelected;
	}
	
	public List<RawDoc> readDocsFromCsv(Path file) throws IOException {
		List<RawDoc> docs = new ArrayList<RawDoc>();
		try (
	            Reader reader = Files.newBufferedReader(file);
	            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
	        ) {
				List<CSVRecord> records = csvParser.getRecords();
	            for (CSVRecord csvRecord : records) {
	            	docs.add(new RawDoc(csvRecord));
	            }
	        }
		return docs;
	}
	
	public void writeDocsToCsv(Path file, List<Result> results) throws IOException {
		try (BufferedWriter writer = Files.newBufferedWriter(file);
				CSVPrinter csvPrinter = new CSVPrinter(writer,
						CSVFormat.DEFAULT.withHeader("DocID", "PredictedClassNumber", "TrueClassNumber"));) {
			for (Result result : results) {
				csvPrinter.printRecord(result.getDocId(), result.getPredictedClassId(), result.getTrueClassId());
			}

			csvPrinter.flush();
		}
	}
	
	public Map<String, String> readClasses(Path file) throws IOException {
		Map<String, String> classes = new HashMap<String, String>();
		try (Stream<String> stream = Files.lines(file)) {
			String[] parts;
			for (String line : (Iterable<String>) stream::iterator) {
				if (line != null && line.length() > 0) {
					parts = line.trim().split(" ");
					classes.put(parts[0], parts[1]);					
				}
			}
		}
		return classes;
	}
}
