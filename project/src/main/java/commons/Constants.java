package commons;

public class Constants {
	public final static int NUM_OF_STOPWORDS = 55;

	public final static int NUM_OF_DOCS_TRAIN = 100000;
	public final static int NUM_OF_DOCS_TEST = 20000;
	
	public static final String INDEX_PATH = "indexPath";
	public static final String TRAIN_FILE = "trainFile";
	public static final String TEST_FILE = "testFile";
	public static final String OUTPUT_FILE = "outputFile";
	public static final String CLASSES_FILE = "classesFile";
	public static final String K = "k";
}
