package index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;

import commons.Utils;

public class Searcher {
	private static final int NUM_OF_RESULT = 12;
	private String indexPath;
	private String retrievalAlgorithm;
	private Analyzer analyzer;
	private IndexSearcher indexSearcher;
	private Similarity sim;
	
	public Searcher(String indexPath, Analyzer analyzer, Similarity sim, String retrievalAlgorithm) {
		setAnalyzer(analyzer);
		setIndexPath(indexPath);
		this.sim = sim;
		this.retrievalAlgorithm = retrievalAlgorithm;
	}
	
	public List<Integer> search(String query) throws IOException, ParseException {
		IndexSearcher indexSearcher = getOrCreateIndexSearcher(indexPath);
		List<Integer> result = new ArrayList<Integer>();
		TopDocs foundDocs = searchByContent(query, indexSearcher, analyzer);
        
		float epsilon = Utils.isBasicMode(retrievalAlgorithm) ? 0.08F : 1.3F;
		float threshold = 0;
		for (ScoreDoc doc : foundDocs.scoreDocs) {
			threshold += doc.score;
		}
		threshold /= foundDocs.scoreDocs.length;
		threshold += epsilon;

        for (ScoreDoc sd : foundDocs.scoreDocs) {
			if (sd.score > threshold) {
				Document d = indexSearcher.doc(sd.doc);
				String docId = d.get(Indexer.DOC_ID);
				result.add(Integer.parseInt(docId));
			}
		}
//		result = result.stream().sorted().collect(Collectors.toList());
		return result;
	}

	public IndexSearcher createIndexSearcher(String indexPath) throws IOException {
		Directory dir = Indexer.getIndexDir(indexPath);
		IndexReader reader = DirectoryReader.open(dir);
		IndexSearcher searcher = new IndexSearcher(reader);
		if (sim != null) {
			searcher.setSimilarity(sim);
		}
		return searcher;
	}
	
	private TopDocs searchByContent(String query, IndexSearcher searcher, Analyzer analyzer) throws ParseException, IOException {
		QueryParser qp = new QueryParser("content", analyzer);
		Query q = qp.parse(query);
		TopDocs hits = searcher.search(q, NUM_OF_RESULT);
		return hits;
	}

	public String getIndexPath() {
		return indexPath;
	}

	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}

	public Analyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}

	public IndexSearcher getOrCreateIndexSearcher(String indexPath) throws IOException {
		if (indexSearcher == null) {
			indexSearcher = createIndexSearcher(indexPath);
		}
		return indexSearcher;
	}
	
	public IndexSearcher getIndexSearcher() {
		return indexSearcher;
	}

	public void setIndexSearcher(IndexSearcher indexSearcher) {
		this.indexSearcher = indexSearcher;
	}
}
