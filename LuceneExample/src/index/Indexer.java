package index;

import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import model.RawDoc;

/**
 *	Holds the logic of indexing documents 
 */
public class Indexer {
	public static final String CONTENT = "content";
	public static final String PAGE_NUMBER = "pageNumber";
	public static final String DATE = "date";
	public static final String DOC_ID = "docId";
	private IndexWriter writer;
	private String indexPath;
	private Similarity sim;

	public Indexer() {
	}
	
	public Indexer(String indexPath) {
		this.indexPath = indexPath;
		init();
	}
	
	public Indexer(String indexPath, Similarity sim) {
		this.indexPath = indexPath;
		this.sim = sim;
		init();
	}
	
	private void init() {
		this.setIndexPath(indexPath);
		try {
			this.setWriter(createIndexerWriter(indexPath, true));
		} catch (IOException e) {
			System.out.println("Failed to create writer...");
		}
	}

	public static Directory getIndexDir(String indexPath) throws IOException {
		Directory dir = null;
		try {
			dir = FSDirectory.open(Paths.get(indexPath));
		} catch (IOException e) {
			System.out.println("Could not create index dir:" + e.getMessage());
			throw e;
		}
		return dir;
	}
	
	public void close() throws IOException {
		if (this.writer != null) {
			this.writer.close();
		}
	}
	
	public void restore() throws IOException {
		this.setWriter(createIndexerWriter(getIndexPath(), false));
	}
	
	private IndexWriter createIndexerWriter(String indexPath, boolean create) throws IOException {
		Directory dir = getIndexDir(indexPath);
		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		if (sim != null) {
			iwc.setSimilarity(sim);
		}
		if (create) {
			iwc.setOpenMode(OpenMode.CREATE);
		} else {
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}

		// Optional: for better indexing performance, if you
		// are indexing many documents, increase the RAM
		// buffer. But if you do this, increase the max heap
		// size to the JVM (eg add -Xmx512m or -Xmx1g):
		//
		// iwc.setRAMBufferSizeMB(256.0);

		IndexWriter writer = new IndexWriter(dir, iwc);

		return writer;
	}

	public void indexDoc(RawDoc newDoc) throws ParseException, IOException {
		Document document = new Document();

		document.add(new StringField(DOC_ID, newDoc.getDocId(), Field.Store.YES));
		document.add(new StringField(DATE, newDoc.getDate(), Field.Store.YES));
		document.add(new StringField(PAGE_NUMBER, newDoc.getPageNumber(), Field.Store.YES));
		
		document.add(new TextField(CONTENT, newDoc.getFormattedContent(), Field.Store.YES));
//		FieldType contentFieldType = new FieldType(TextField.TYPE_NOT_STORED);
//	    contentFieldType.setStoreTermVectors(true);
//	    contentFieldType.setStoreTermVectorPositions(true);
//	    contentFieldType.setStoreTermVectorPayloads(true);
//	    contentFieldType.setStoreTermVectorOffsets(true);
//	    contentFieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
//	    Field contentField = new Field(CONTENT, newDoc.getFormattedContent(), contentFieldType);
//	    document.add(contentField);
	    
		writer.addDocument(document);
		
		System.out.println(newDoc.getDocId() + " " + newDoc.getDate() + " PAGE " + newDoc.getPageNumber() + " : INDEXED");
	}

	public IndexWriter getWriter() {
		return writer;
	}

	public void setWriter(IndexWriter writer) {
		this.writer = writer;
	}

	public String getIndexPath() {
		return indexPath;
	}

	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}
}
