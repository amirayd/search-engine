package model;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.Similarity;

public class EngineContext {
	private List<RawDoc> docs;
	private Set<Object> stopWords;
	private Analyzer analyzer;
	private List<RawQuery> queries;
	private Map<String, String> paths;
	private int numOfStopWords;
	private Similarity sim;
	private String retrievalAlgorithm;
	
	public List<RawDoc> getDocs() {
		return docs;
	}
	public void setDocs(List<RawDoc> docs) {
		this.docs = docs;
	}
	public Set<Object> getStopWords() {
		return stopWords;
	}
	public void setStopWords(Set<Object> stopWords) {
		this.stopWords = stopWords;
	}
	public Analyzer getAnalyzer() {
		return analyzer;
	}
	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}
	public List<RawQuery> getQueries() {
		return queries;
	}
	public void setQueries(List<RawQuery> queries) {
		this.queries = queries;
	}
	public Map<String, String> getPaths() {
		return paths;
	}
	public void setPaths(Map<String, String> paths) {
		this.paths = paths;
	}
	public int getNumOfStopWords() {
		return numOfStopWords;
	}
	public void setNumOfStopWords(int numOfStopWords) {
		this.numOfStopWords = numOfStopWords;
	}
	public Similarity getSim() {
		return sim;
	}
	public void setSim(Similarity sim) {
		this.sim = sim;
	}
	
	public String getRetrievalAlgorithm() {
		return retrievalAlgorithm;
	}
	public void setRetrievalAlgorithm(String retrievalAlgorithm) {
		this.retrievalAlgorithm = retrievalAlgorithm;
	}
}
