package model;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.TokenStream;

import services.ScoreCalculator;

/**
 * Represent a raw query
 * RawQuery.Score -> inner class for scoring 
 */
public class RawQuery {

	private String id;
	private StringBuffer content;
	private String formattedContent;
	private TokenStream stream;
	private List<Integer> expectedResults;
	private List<Integer> actualResults;
	private Score score;

	/**
	 * 
	 * @param queryConfig
	 *            - in format: "*FIND <ID>"
	 */
	public RawQuery(String queryConfig) {
		String[] parts = queryConfig.split("\\s+");
		setId(parts[1]);
		content = new StringBuffer();
		expectedResults = new ArrayList<Integer>();
		actualResults = new ArrayList<Integer>();
	}
	
	public void addContent(String line) {
		if (line != null) {
			// content.append("\n");
			content.append(line);
		}
	}

	public String getContent() {
		return content.toString();
	}

	public String getFormattedContent() {
		return formattedContent;
	}

	public void setFormattedContent(String formattedContent) {
		this.formattedContent = formattedContent;
	}

	public TokenStream getStream() {
		return stream;
	}

	public void setStream(TokenStream stream) {
		this.stream = stream;
	}

	public List<Integer> getExpectedResults() {
		return expectedResults;
	}
	
	public void setExpectedResults(List<Integer> expectedResults) {
		this.expectedResults = expectedResults;
	}

	public List<Integer> getActualResults() {
		return actualResults;
	}

	public void setActualResults(List<Integer> actualResults) {
		this.actualResults = actualResults;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Score getScore() {
		return score;
	}
	
	public void score(List<Integer> intersect) {
		this.score = new Score(actualResults, expectedResults, intersect);
	}
	
	public String getActualResultsFormatted() {
		StringBuilder sb = new StringBuilder();
		sb.append(getId());
		sb.append(" ");
		for (Integer docId : getActualResults()) {
			sb.append(docId);
			sb.append(" ");
		}
		return sb.toString();		
	}
	
	public class Score {
		private int numOfAcutalResults;
		private int numOfExpectedResults;
		private int numOfCorrect;
		private double precision;
		private double recall;
		private double fscore;
		private double alpha = 0.5;
		private double beta = 1;

		public Score(List<Integer> actualResults, List<Integer> expectedResults, List<Integer> intersect) {
			this.numOfAcutalResults = actualResults.size();
			this.numOfExpectedResults = expectedResults.size();
			this.numOfCorrect = intersect.size();
			double[] calculated = ScoreCalculator.calculate(actualResults, expectedResults, intersect, alpha, beta);
			recall = calculated[0];
			precision = calculated[1];
			fscore = calculated[2];
		}

		public void printScore() {
			StringBuilder sb = new StringBuilder();
			sb.append("\n#");
			sb.append(getId());
			sb.append(": correct=");
			sb.append(getNumOfCorrect());
			sb.append(", expected=");
			sb.append(getNumOfExpectedResults());
			sb.append(", actual=");
			sb.append(getNumOfAcutalResults());
			sb.append(", precision=");
			sb.append(precision);
			sb.append(", recall=");
			sb.append(recall);
			sb.append(", fscore=");
			sb.append(getFscore());
			System.out.println(sb.toString());
		}
		
		public String getActualScoreFormatted() {
			StringBuilder sb = new StringBuilder();
			sb.append(getId());
			sb.append(" ");
			sb.append(precision);
			sb.append(" ");
			sb.append(recall);
			sb.append(" ");
			sb.append(getFscore());
			return sb.toString();		
		}
		
		public double getPrecision() {
			return precision;
		}
		
		public double getRecall() {
			return recall;
		}
		
		public double getFscore() {
			return fscore;
		}

		public void setFscore(double fscore) {
			this.fscore = fscore;
		}

		public int getNumOfAcutalResults() {
			return numOfAcutalResults;
		}
		
		public int getNumOfExpectedResults() {
			return numOfExpectedResults;
		}
		
		public int getNumOfCorrect() {
			return numOfCorrect;
		}

	}

}
