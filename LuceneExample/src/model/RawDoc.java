package model;

/**
 *  Represent a raw document
 */
public class RawDoc {
	private String docId;
	private String date;
	private String pageNumber;
	private StringBuffer content;
	private String formattedContent;
//	private TokenStream stream;
	
	/**
	 * 
	 * @param docConfig - in format: "*TEXT <DocID> <Date> PAGE <number>"
	 */
	public RawDoc(String docConfig) {
		String[] parts = docConfig.split("\\s+");
		docId = parts[1];
		date = parts[2];
		pageNumber = parts[4];
		content = new StringBuffer();
	}
	
	public void addContent(String line) {
		if (line != null) {
//			content.append("\n");
			content.append(line);			
		}
	}
	
	public String getContent() {
		return content.toString();
	}
	
	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getFormattedContent() {
		return formattedContent;
	}

	public void setFormattedContent(String formattedContent) {
		this.formattedContent = formattedContent;
	}
//
//	public TokenStream getStream() {
//		return stream;
//	}
//
//	public void setStream(TokenStream stream) {
//		this.stream = stream;
//	}

}
