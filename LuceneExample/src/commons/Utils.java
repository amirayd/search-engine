package commons;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class Utils {
	
	public static Set<Object> mapToSet(ConcurrentMap<String, Integer> words, int limit) {
		LinkedHashMap<Object, Object> sortedAndLimited = 
				words.entrySet().stream()
			    .sorted((o1, o2) -> o2.getValue() - o1.getValue())
			    .limit(limit)
			    .collect(Collectors.toMap(Entry::getKey, Entry::getValue,
			                              (e1, e2) -> e1, LinkedHashMap::new));
		return sortedAndLimited.keySet();
	}

	public static <T> List<T> intersect(List<T> list1, List<T> list2) {
		if (list1 != null && list2 != null) {
			return list1.stream()
			        .filter(list2::contains)
			        .collect(Collectors.toList());
		}
		return null;
	}
	
	public static boolean isBasicMode(String algo) {
		return algo != null && algo.equals("basic");
	}
}
