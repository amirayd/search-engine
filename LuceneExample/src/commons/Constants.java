package commons;

public class Constants {
	public final static int NUM_OF_STOPWORDS = 20;
	public final static int NUM_OF_STOPWORDS_ADVANCED = 55;
	
	public static final String RETRIEVAL_ALGORITHM = "retrievalAlgorithm";
	public static final String ACTUAL_SCORE_RESULTS = "actualScoreFile";
	public static final String TRUTH_FILE = "truthFile";
	public static final String INDEX_PATH = "indexPath";
	public static final String QUERY_FILE = "queryFile";
	public static final String DOCS_FILE = "docsFile";
	public static final String OUTPUT_FILE = "outputFile";
}
