import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;

import commons.Constants;
import commons.Utils;
import index.Indexer;
import index.Searcher;
import index.TokenManager;
import model.EngineContext;
import model.RawDoc;
import model.RawQuery;
import services.InputProcessor;
import services.ResultsEvaluator;

public class ContextRunner {

	private EngineContext context;
	private TokenManager tokenManager;
	private ResultsEvaluator evaluator;
	
	public ContextRunner() {
		this.tokenManager = new TokenManager();
		this.evaluator = new ResultsEvaluator();
	}
	
	public void run() throws IOException {
		run(this.context);
	}
	
	public void run(EngineContext context) throws IOException {
		if (context == null) {
			throw new IllegalArgumentException("context is not defined");
		}
		this.context = context;
		
		context.setDocs(new ArrayList<RawDoc>());
		
		// 1. pre-processing - reading docs
		preprocess();
		
		context.setAnalyzer(new EnglishAnalyzer());
		
		// in advanced mode we are using BM25Similarity (default), which is generally considered superior to TF-IDF.
		if (Utils.isBasicMode(context.getRetrievalAlgorithm())) {
			context.setSim(new ClassicSimilarity());			
		} else {
			context.setSim(new BM25Similarity());
		}
		
		// 2. tokenize + remove stop words + stemming
		tokenManager.createTokenStreamsWithFilters(context);
		
		// 3. indexing
		index();
		
		// 4. searching
		context.setQueries(search(context));
	
		// 5. evaluating results
		Map<String, Double> sums = evaluator.evaluateResults(context);
		
		printSums(sums);
		
		context.getAnalyzer().close();
	}

	private void preprocess() throws IOException {
		ConcurrentMap<String, Integer> words = new ConcurrentHashMap<>();
		// creating RawDoc for each document and count the most frequent words
		InputProcessor.processRawDocs(Paths.get(context.getPaths().get(Constants.DOCS_FILE)), 
				(doc) -> processRawDoc(context.getDocs(), doc), (line) -> tokenManager.countWordsInLine(line, words));
		context.setStopWords(Utils.mapToSet(words, context.getNumOfStopWords()));
	}

	private void index() throws IOException {
		Indexer indexer = new Indexer(context.getPaths().get(Constants.INDEX_PATH), context.getSim());
		System.out.println("\n\n\nindexing:");
		context.getDocs().forEach(doc -> indexDoc(indexer, doc));
		indexer.close();
	}

	private List<RawQuery> search(EngineContext context) throws IOException {
		List<RawQuery> queries = new ArrayList<RawQuery>();
		Searcher searcher = new Searcher(context.getPaths().get(Constants.INDEX_PATH), context.getAnalyzer(), context.getSim(), context.getRetrievalAlgorithm());
		InputProcessor.processQueries(Paths.get(context.getPaths().get(Constants.QUERY_FILE)), 
				(query) -> processQuery(query, context, searcher, queries), null);
		return queries;
	}

	private RawDoc processRawDoc(List<RawDoc> docs, RawDoc doc) {
		if (docs != null) {
			docs.add(doc);
		}
		return doc;
	}

	private RawDoc indexDoc(Indexer indexer, RawDoc doc) {
		if (doc != null) {
			try {
				indexer.indexDoc(doc);
			} catch (java.text.ParseException | IOException e) {
				System.out.println("(indexDoc) Error:" + e.getMessage());
				e.printStackTrace();
			}
		}
		return doc;
	}
	
	private List<Integer> processQuery(RawQuery query, EngineContext context, Searcher searcher, List<RawQuery> queries) {
		List<Integer> result = null;
		if (query != null) {
			try {
				String formatted = tokenManager.tokenStreamToString(
						tokenManager.tokenizeStopStem(query.getContent(), context));
				result = searcher.search(formatted);
				query.setActualResults(result);
				queries.add(query);
			} catch (IOException | ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Error: " + e.getMessage());
			}
		}
		
		return result;
	}
	
	private void printSums(Map<String, Double> sums) {
		System.out.println("");
		System.out.println("FScore -> " + (sums.get("fscore") / context.getQueries().size()));
		System.out.println("Precision -> " + (sums.get("precision") / context.getQueries().size()));
		System.out.println("Recall -> " + (sums.get("recall") / context.getQueries().size()));
		System.out.println("");
	}

	public EngineContext getContext() {
		return context;
	}

	public void setContext(EngineContext context) {
		this.context = context;
	}

	public TokenManager getTokenManager() {
		return tokenManager;
	}

	public void setTokenManager(TokenManager tokenManager) {
		this.tokenManager = tokenManager;
	}
}
