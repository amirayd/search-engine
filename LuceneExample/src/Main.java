
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.queryparser.classic.ParseException;

import commons.Constants;
import commons.Utils;
import model.EngineContext;
import services.InputProcessor;

public class Main {

	
	
	public static void main(String[] args) throws IOException, ParseException {
		ClassLoader classLoader = Main.class.getClassLoader();
		Map<String, String> params = InputProcessor.readParamsFile(Paths.get(classLoader.getResource("parameters.txt").getPath()));
		
		EngineContext context = new EngineContext();
		// load algorithm mode and remove it from params
		context.setRetrievalAlgorithm(params.remove(Constants.RETRIEVAL_ALGORITHM));
		
		normalizePaths(params);
		// all related files
		context.setPaths(params);
		
		// configurations
		final int numOfStopWOrds = Utils.isBasicMode(context.getRetrievalAlgorithm()) ? Constants.NUM_OF_STOPWORDS : Constants.NUM_OF_STOPWORDS_ADVANCED;
		context.setNumOfStopWords(numOfStopWOrds);
		
		Date start = new Date();
		
		new ContextRunner().run(context);
		
		System.out.println(new Date().getTime() - start.getTime() + " total milliseconds");
	}

	private static void normalizePaths(Map<String, String> paths) {
		ClassLoader classLoader = Main.class.getClassLoader();
		paths.put(Constants.ACTUAL_SCORE_RESULTS, "out/actualScore.txt");
		if (paths.containsKey(Constants.INDEX_PATH)) { // adding index path
			paths.put(Constants.INDEX_PATH, "index");
		}
		if (paths.containsKey(Constants.TRUTH_FILE)) { // adding truth file
			paths.put(Constants.TRUTH_FILE, "data/truth.txt");
		}
		String key;
		for (Entry<String, String> entry : paths.entrySet()) {
			key = entry.getKey();
			if (key != null && key.endsWith("File") || key.endsWith("Path")) {
				URL resource = classLoader.getResource(entry.getValue());
				if (resource != null) {
					entry.setValue(resource.getPath());						
				}
			}
		}
	}

}
