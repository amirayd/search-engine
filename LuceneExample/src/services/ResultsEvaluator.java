package services;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import commons.Constants;
import commons.Utils;
import model.EngineContext;
import model.RawQuery;

public class ResultsEvaluator {

	public Map<String, Double> evaluateResults(EngineContext context) throws IOException {
		Map<String, Double> sums = new HashMap<String, Double>();
		
		InputProcessor.processQueriesResults(Paths.get(context.getPaths().get(Constants.TRUTH_FILE)), 
				(line) -> processQueryResult(line, context.getQueries()));
		context.getQueries().forEach((query) -> score(query));
		List<String> actualRaw = context.getQueries()
				.stream()
				.map((query) -> query.getActualResultsFormatted())
				.collect(Collectors.toList());
		InputProcessor.writeActualResults(context.getPaths().get(Constants.OUTPUT_FILE), actualRaw, "");
		
		List<String> actualScore = context.getQueries()
				.stream()
				.map((query) -> query.getScore().getActualScoreFormatted())
				.collect(Collectors.toList());
		actualScore.add(0, "DocID Precision Recall F-Score");
		InputProcessor.writeActualResults(context.getPaths().get(Constants.ACTUAL_SCORE_RESULTS), actualScore, "");
		
		// summary of results for console...
		sums.put("fscore", context.getQueries().stream().mapToDouble((query) -> query.getScore().getFscore()).sum());
		sums.put("precision", context.getQueries().stream().mapToDouble((query) -> query.getScore().getPrecision()).sum());
		sums.put("recall", context.getQueries().stream().mapToDouble((query) -> query.getScore().getRecall()).sum());
		
		return sums;
	}

	private void score(RawQuery query) {
		if (query != null) {
			List<Integer> intersect = Utils.intersect(query.getActualResults(), query.getExpectedResults());
			query.score(intersect);
			query.getScore().printScore();
		}
	}

	private RawQuery processQueryResult(String line, List<RawQuery> queries) {
		String[] parts = line.trim().split("[^0-9']+");
		int id = Integer.parseInt(parts[0]);
		RawQuery query = queries.get(id - 1);
		if (query != null) {
			for (int i = 1; i < parts.length; i++) {
				query.getExpectedResults().add(Integer.parseInt(parts[i]));
			}
		}
		return query;
	}

}
