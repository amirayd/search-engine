package services;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import model.RawDoc;
import model.RawQuery;

public class InputProcessor {
	
	public static Map<String, String> readParamsFile(Path paramsFile) throws IOException {
		try (Stream<String> stream = Files.lines(paramsFile)) {
			Map<String, String> params = new HashMap<String, String>();
			for (String line : (Iterable<String>) stream::iterator) {
				if (line != null && line.length() > 0) {
					String[] parts = line.split("=");
					if (parts.length > 0) {
						params.put(parts[0], parts[1]);
					}
				}
			}
			return params;
		}
	}
	
	public static void processRawDocs(Path docsFile, Function<RawDoc, RawDoc> iterator, Function<String, String> lineIterator) throws IOException {
		try (Stream<String> stream = Files.lines(docsFile)) {
			RawDoc doc = null;
			for (String line : (Iterable<String>) stream::iterator) {
				if (line == null) {
					// do nothing
				} else if (line.startsWith("*TEXT")) {
					iterator.apply(doc); // iterating previous doc before creating new one
					// creates a new doc
					doc = new RawDoc(line);
				} else if (line.startsWith("*STOP")) {
					iterator.apply(doc); // iterating last doc
					System.out.println("End of file...");
				} else {
					if (lineIterator != null) {
						line = lineIterator.apply(line);
					}
					doc.addContent(line);
				}
			}
		}
	}
	
	public static void processQueries(Path queriesFile, Function<RawQuery, List<Integer>> iterator, Function<String, String> lineIterator) throws IOException {
		try (Stream<String> stream = Files.lines(queriesFile)) {
			RawQuery query = null;
			for (String line : (Iterable<String>) stream::iterator) {
				if (line == null) {
					// do nothing
				} else if (line.startsWith("*FIND")) {
					iterator.apply(query); // iterating previous doc before creating new one
					// creates a new doc
					query = new RawQuery(line);
				} else {
					if (lineIterator != null) {
						line = lineIterator.apply(line);
					}
					query.addContent(line);
				}
			}
			iterator.apply(query); // last query...
		}
	}
	
	public static void processQueriesResults(Path truthFile, Function<String, RawQuery> iterator) throws IOException {
		try (Stream<String> stream = Files.lines(truthFile)) {
			for (String line : (Iterable<String>) stream::iterator) {
				if (line == null || line.trim().length() == 0) {
					// do nothing
				} else {
					iterator.apply(line.trim());
				}
			}
		}
	}
	
	public static void writeActualResults(String actualResults, List<String> actual, String prefix) {
		Path path = Paths.get(actualResults + prefix);
		try {
			StandardOpenOption opts = StandardOpenOption.CREATE_NEW;
			if (Files.exists(path)) {
				opts = StandardOpenOption.CREATE;
			}
			Files.write(path, actual, Charset.defaultCharset(), opts);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
