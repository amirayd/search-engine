package services;

import java.util.List;

public class ScoreCalculator {

	public static double[] calculate(List<Integer> actualResults, List<Integer> expectedResults, List<Integer> intersect, double alpha, double beta) {
		double recall = 0, precision = 0, fscore = 0;
		double nIntersect = intersect.size();
		if (expectedResults.size() == 0) {
			if (actualResults.size() == 0) {
				recall = precision = fscore = 1;
			} else {
				recall = 1;
				precision = fscore = 0;
			}
		} else if (nIntersect == 0) { // no match..
			recall = precision = fscore = 0;
		} else {
			double fn = expectedResults.size() - nIntersect;
			double fp = actualResults.size() - nIntersect;
			recall = (nIntersect) / (nIntersect + fn);
			precision = (nIntersect) / (nIntersect + fp);
			// FSCORE
			fscore = 2 / ((1 / precision) + (1 / recall));
		}
		double[] results = { recall, precision, fscore };
		return results;
	}
}
